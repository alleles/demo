SHELL ?= /bin/bash
PAGER ?= less
.DEFAULT_GOAL := help

_IGNORE_VARS =

########################
##### General vars #####
########################

export UID_GID ?= $(shell id -u):$(shell id -g)

########################
##### Alleles vars #####
########################

ALLELES_URL = https://gitlab.com/alleles
ALLELES_REGISTRY = registry.gitlab.com/alleles

ANNO_REPO = ${ALLELES_URL}/ella-anno.git
ANNO_VERSION ?= $(shell ./bin/latest_tag.sh -n anno)
ANNO_IMAGE ?= ${ALLELES_REGISTRY}/ella-anno:${ANNO_VERSION}
ANNOBUILDER_IMAGE ?= ${ALLELES_REGISTRY}/ella-anno:builder-${ANNO_VERSION}
ANNO_MAKEFILE = init/anno_${ANNO_VERSION}.mk
ANNO_URL = ${ALLELES_URL}/ella-anno/-/raw/${ANNO_VERSION}
ANNO_DATA ?= ${CURDIR}/anno-data

ELLA_REPO = ${ALLELES_URL}/ella.git
ELLA_VERSION ?=  $(shell ./bin/latest_tag.sh -n ella)
ELLA_IMAGE ?= ${ALLELES_REGISTRY}/ella:${ELLA_VERSION}
TESTDATA_GIT = ${CURDIR}/ella-testdata/.git

######################
##### Validation #####
######################

ifeq (,${ANNO_DATA})
ANNO_DATA = ${CURDIR}/anno-data
$(info "Empty ANNO_DATA directory specified, using ${ANNO_DATA}")
endif

###################
##### Recipes #####
###################
.PHONY: pull pull-ella pull-anno up

### Primary

pull: pull-anno pull-ella ## pull the alleles docker images

init: init-anno init-ella ## downloads and configures necessary files

up: pull init ## starts the compose stack
	docker-compose up -d --wait

### Anno

pull-anno:
	docker pull ${ANNO_IMAGE}

init-anno: ${ANNO_MAKEFILE} ${ANNO_DATA}

${ANNO_MAKEFILE}:
	mkdir -p init
	curl -s -L ${ANNO_URL}/Makefile >${ANNO_MAKEFILE}

${ANNO_DATA}:
	mkdir -p anno-data
	docker pull ${ANNOBUILDER_IMAGE}
	${MAKE} -f ${ANNO_MAKEFILE} download-data \
		RELEASE_TAG=${ANNO_VERSION} \
		ANNO_DATA=${ANNO_DATA} \
		RUN_CMD_ARGS="--skip-validation --force" \
		USE_REGISTRY=1

### ELLA

pull-ella:
	docker pull ${ELLA_IMAGE}

init-ella: ${TESTDATA_GIT}

${TESTDATA_GIT}:
	git submodule sync
	git submodule update --init --recursive

################################
##### Help text generation #####
################################

##---------------------------------------------
## Help / Debugging
##  Other vars: VAR_ORIGIN, FILTER_VARS
##---------------------------------------------
.PHONY: help vars local-vars

# only use ASCII codes if running in terminal (e.g., not when piped)
ifneq ($(MAKE_TERMOUT),)
_CYAN = \033[36m
_RESET = \033[0m
endif

# Using the automatic `make help` generation:
# Lines matching /^## / are considered section headers
# Use a ## at the end of a rule to have it printed out
# e.g., `some_rule: ## some_rule help text` at the end of a rule to have it included in help output
help: ## prints this help message
	@grep -E -e '^[a-zA-Z_-]+:.*?## .*$$' -e '^##[ -]' $(MAKEFILE_LIST) \
		| awk 'BEGIN {prev = ""; FS = ":.*?## "}; {if (match($$1, /^#/) && match(prev, /^#/) == 0) printf "\n"; printf "$(_CYAN)%-30s$(_RESET) %s\n", $$1, $$2; prev = $$1}'
	@echo
	@echo "Additional comments available in this Makefile\n"

NULL_STRING :=
BLANKSPACE = $(NULL_STRING) # this is how we get a single space
_IGNORE_VARS += NULL_STRING BLANKSPACE
vars: _list_vars ## prints out variables available in the Makefile and the origin of their value
	@true

# actually prints out the desired variables, should not be called directly
# uses environment and environment_override by default, always includes file and command_line
# ref:
#        origin:  https://www.gnu.org/software/make/manual/html_node/Origin-Function.html#Origin-Function
#    .VARIABLES:  https://www.gnu.org/software/make/manual/html_node/Special-Variables.html#Special-Variables
# overall magic:  https://stackoverflow.com/a/59097246/5791702
FILTER_VARS ?=
_list_vars:
	$(eval filtered_vars = $(sort $(filter-out $(sort $(strip $(FILTER_VARS) $(_IGNORE_VARS))),$(.VARIABLES))))
	$(eval VAR_ORIGIN ?= environment environment_override)
	$(eval override VAR_ORIGIN += file command_line)
	$(foreach v, $(filtered_vars), $(if $(filter $(sort $(VAR_ORIGIN)),$(subst $(BLANKSPACE),_,$(origin $(v)))), $(info $(v) ($(origin $(v))) = $($(v)))))

_disable_origins:
	$(eval VAR_ORIGIN = )

ENV = $(shell command -v env)
local-vars: _disable_origins _list_vars ## print out vars set by command line and in the Makefile
	@true
