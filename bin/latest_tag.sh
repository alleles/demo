#!/bin/bash -e

show_help() {
    echo "Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS]"
    echo
    echo "    Shows the latest release tag for the repo"
    echo
    echo "  -n <name>        Repo name. One of: ${!REPO_URLS[*]}"
    echo "  -a               Show all tags "
    echo "  -h               Show this message"
    echo
    return 1
}

get_tags() {
    local repo_url=$1
    mapfile -t repo_tags < <(
        git ls-remote --tags "$repo_url" \
            | grep -oE 'refs/tags/v?[0-9]+\.[0-9]+\.[0-9]+$' \
            | cut -f3 -d/ \
            | sort -rV
    )

    if [[ -n ${VERBOSE:-} ]]; then
        echo >&2 "Found ${#repo_tags[@]} tags in $repo_url"
    fi
}

validate_repo() {
    local repo=$1
    if [[ -z $repo ]]; then
        echo >&2 "Repo name is required"
        return 1
    elif [[ -z ${REPO_URLS[$repo]} ]]; then
        echo >&2 "ERROR - unknown repo: $repo"
        return 1
    fi
}

BASE_URL=https://gitlab.com/alleles
declare -A REPO_URLS=(
    [ella]=${BASE_URL}/ella.git
    [anno]=${BASE_URL}/ella-anno.git
)

while getopts ':n:ah' opt; do
    case "$opt" in
        n)
            REPO_NAME=$OPTARG
            ;;
        a)
            ALL=1
            ;;
        h)
            show_help
            exit 1
            ;;
        :)
            echo "Error: -$OPTARG requires an argument."
            exit 1
            ;;
        *)
            echo "Unknown option: -$OPTARG"
            exit 1
            ;;
    esac
done

validate_repo "$REPO_NAME"

get_tags "${REPO_URLS[$REPO_NAME]}"
if [[ -n $ALL ]]; then
    # list is in reverse order, so reverse it again
    printf '%s\n' "${repo_tags[@]}" | tac
else
    printf '%s\n' "${repo_tags[0]}"
fi
